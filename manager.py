import autoit


class ApplicationManager():
    def __init__(self):
        autoit.run("mstsc.exe")
        autoit.win_wait_active("[CLASS:#32770]", 3)

    def close(self):
        autoit.win_close("[CLASS:#32770]")


app_manager = ApplicationManager()
