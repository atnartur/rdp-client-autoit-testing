# -*- coding: utf-8 -*-
import time

import autoit
import unittest

from base import TestBase
from manager import app_manager


class MainTest(TestBase, unittest.TestCase):
    def test_main(self):
        autoit.control_send("[CLASS:#32770]", "Edit1", "test")  # вводим хостнейм
        autoit.control_click("[Class:#32770]", "Button5")  # нажимаем на кнопку подключения
        time.sleep(3)  # происходит подключение, немного посмотрим на этот процесс
        autoit.control_click("[Class:#32770]", "Button1")  # нажмем на кнопку отмены
        time.sleep(1)  # покажем, что все закрылось и завершим тест


def tearDownModule():
    app_manager.close()


if __name__ == "__main__":
    unittest.main()
