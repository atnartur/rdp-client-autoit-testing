# AutoIt Windows RDP client testing

Тестирует стандартный RDP клиент на Windows с помощью [AutoIt](https://www.autoitscript.com/site/autoit/)

Требует установки Python 3 и выполнения `pip install pyautoit`

Запуск: `python run.py`
